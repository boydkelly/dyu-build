#!/usr/bin/bash
UI=antora-ui-default
UI=web-docs-ui
export FORCE_SHOW_EDIT_PAGE_LINK="true"

[[ -d public ]] && rm -rf public
[[ -f ../${UI}/build/ui-bundle.zip ]] || { echo bundle not found ; pushd ../${UI} && SOURCEMAPS=true gulp bundle  && popd ; }

#pushd ../${UI} && env SOURCEMAPS=true gulp bundle  && popd ;

for x in npx po4a; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not found.  Aborting."; exit 1; }
echo
done

echo Using $UI
export GITHUB=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
export DOCSAUTH=ip9nZynj-sVUW1tyNstL
export FORCE_SHOW_EDIT_PAGE_LINK="true"
echo $DOCSAUTH
sed s/\$DOCSAUTH/"$DOCSAUTH"/g local.yml > tmp.yml


cp -v ../${UI}/build/ui-bundle.zip .
npx antora --stacktrace --fetch $1 tmp.yml
gzip -k -9 $(find public -iname '*.html' -o -iname '*.css' -o -iname '*.js' -o -iname '*.xml' -o -iname '*.woff2')
brotli -Z $(find public -iname '*.html' -o -iname '*.css' -o -iname '*.js' -o -iname '*.xml' -o -iname '*.woff2')

rm tmp.yml
cd public && python -m http.server 8080
