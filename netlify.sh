#!/usr/bin/bash
UI=antora-ui-default
UI=web-docs-ui
[[ -d ../${UI}/ ]] || { echo theme not found ; exit 1 ; } 
[[ -f ../${UI}/build/ui-bundle.zip ]] || { echo bundle not found ; pushd ../${UI} && SOURCEMAPS=true gulp bundle  && popd ; } 

for x in npx; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not found.  Aborting."; exit 1; }
echo
done

nvm use 16.13.1

echo Using $UI
export AUTH=vfMcSRUneR48zs_iP8gZ
export FORCE_SHOW_EDIT_PAGE_LINK="true"
echo $AUTH
sed s/\$AUTH/"$AUTH"/g local.yml > tmp.yml

pushd ../web-docs
git commit -a -m "update by script on $(date)" && git push origin master
popd

curl -o local.zip -L https://gitlab+deploy-token-439586:$AUTH@gitlab.com/boydkelly/web-docs/-/jobs/artifacts/main/download?job=lexicon
unzip -o -q local.zip
pushd local && git init -b main && git add * && git commit -a -m initial
popd

cp -v ../${UI}/build/ui-bundle.zip .
npx antora --redirect-facility=netlify --stacktrace --fetch tmp.yml
#	git commit -a -m "Built on $(date)" && git push origin master
	#--cache-dir=./.cache  \
rm  tmp.yml
#cd public && python -m http.server 8080

#git commit -a -m "Built on $(date)" && git push origin mster
#local.yml > antora.log
