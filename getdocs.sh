#!/usr/bin/bash

for x in curl; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not found.  Aborting."; exit 1; }
echo
done
git config --global user.name "Boyd Kelly"
git config --global user.email "bkelly@coastsystems.net"
#git config --global url."https://gitlab+deploy-token-338933:$AUTH@gitlab.com".insteadOf "https://gitlab.com"
#curl -o local.zip -L https://gitlab+deploy-token-439586:$AUTH@gitlab.com/boydkelly/web-docs/-/jobs/artifacts/main/download?job=lexicon
wget https://gitlab+deploy-token-1429155:$DOCSAUTH@gitlab.com/boydkelly/web-docs/-/jobs/artifacts/main/download?job=lexicon -O local.zip
[[ -d local ]] && rm -fr local
unzip -o -q local.zip
pushd local && git init -b main && git add * && git commit -a -m initial
popd
